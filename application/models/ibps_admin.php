<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ibps_admin extends CI_model {
    public function insert($table,$data){
       return $this->db->insert($table,$data);
    }

    public function select($table){
        $result = $this->db->get($table)->result();
        return $result;
    }
    public function get_by_id($table,$id){
        $this->db->select('*');
        $this->db->from($table);
        $this->db->where('id',$id);
        $result = $this->db->get()->row();
        // echo $this->db->last_query();
        // print_r($result);exit;
        return $result;
    }
    public function update_data($table,$id,$data){
        $this->db->where('id', $id);
        $this->db->update($table, $data);

    }
    public function delete($table,$id){
        $this->db->delete($table,['id'=>$id]);
       /// echo $this->db->last_query();exit; 
    }
    public function select2(){
        $this->db->select("subject.sub_name,questions.*");
        $this->db->from('questions');
        $this->db->join('subject', 'subject.id = questions.sub_id');
        $result = $this->db->get()->result();
      //  echo $this->db->last_query();exit;
        return $result;
    }
    public function select_login($username){
        $this->db->select('*');
        $this->db->from('users');
        $this->db->where('email',$username);
        $result = $this->db->get()->row();
        return $result;       
    }
}