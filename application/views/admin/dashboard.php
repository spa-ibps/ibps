<div class="container">
    <div class="card">
        <div class="card-header">
            <button><a href="<?= base_url('admin/page')?>">Add Subject</a></button>
            <button><a href="<?= base_url('admin/question')?>">Add questions</a></button>
            <button><a href="<?php echo base_url('admin/showquestion')?>">question List</a></button>
           
            <h1>List of questions and subjects</h1>
        </div>
        <div class="card-body">
            <table class="table">
                <tr>
                    <th>Sr.No</th>
                    <th>Subject name</th>
                    <th>Action</th>
                </tr>
                <?php $s=0; foreach($data as $value) { $s++;?>
                <tr>
                    <td><?php echo $s?></td>
                    <td><?php echo $value->sub_name; ?></td>
                    <td><a href="<?php echo base_url()?>Admin/edit_sub/<?php echo  $value->id ?>">Edit</a> | <a href="<?php echo base_url()?>Admin/delete_sub/<?php echo  $value->id ?>">Delete</a> </td>
                </tr>
                <?php } ?>
            </table>
        </div>
    </div>
</div>