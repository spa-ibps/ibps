<div class="container">
    <div class="card">
        <div class="card-header">
            <button><a href="<?= base_url('admin/question')?>">Add questions</a></button>
            <button><a href="<?php echo base_url('admin/showquestion')?>">question List</a></button>
            <button><a href="<?php echo base_url('admin/dashboard')?>">Subject List</a></button>
            <h1>List of questions </h1>
        </div>
        <div class="card-body">
            <table class="table">
                <tr>
                    <th>Sr.No</th>
                    <th>Subject </th>
                    <th>questions</th>
                    <th>Action</th>
                </tr>
                <?php $s=0; foreach($data as $value) { $s++;?>
                <tr>
                    <td><?php echo $s?></td>
                    <td><?php echo $value->sub_name; ?></td>
                    <td><?php echo $value->question; ?></td>
                    <td><a href="<?php echo base_url()?>Admin/edit_questions/<?php echo  $value->id ?>">Edit</a> | <a href="<?php echo base_url()?>Admin/delete_questions/<?php echo  $value->id ?>">Delete</a> </td>
                </tr>
                <?php } ?>
            </table>
        </div>
    </div>
</div>