<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('user_model');
	}

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index(){
		
		
		$data['result'] = $this->user_model->selectData();
		
		$this->load->view('common/user_header');
		$this->load->view('common/body',$data);
		$this->load->view('common/user_footer');
		
	}
	public function select_Subject(){
	
		//$id = $_POST['id'];
		
		$data = $this->user_model->selectQuetion($_POST['id']);
		
			echo json_encode($data);
		
	}

}
