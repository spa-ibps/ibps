<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {
    public function page(){
        $this->load->view('admin/header');
        $this->load->view('admin/add_subject');
    }

    public function insertSubject(){ 
        $this->load->library('form_validation');
        $this->form_validation->set_rules('subject', 'subject', 'required|is_unique[subject.sub_name]');
        if ($this->form_validation->run() == FALSE)
        {
            $this->load->view('admin/header');
            $this->load->view('admin/add_subject');
        }
        else
        {
            $this->load->model('ibps_admin');
            $data = array(
                        'sub_name' => $_POST['subject']
                    );
           $result = $this->ibps_admin->insert('subject',$data);
           redirect(base_url().'admin/dashboard');

        }
    }

    public function question(){
        $this->load->library('session');
        $this->load->model('ibps_admin');
        $data['data'] = $this->ibps_admin->select('subject');
        $this->load->view('admin/header');
        $this->load->view('admin/add_questions',$data);
    }

    public function insertQuestion(){
        $this->load->library('session');
            $this->load->model('ibps_admin');
            $data = array(
                        'question' => $_POST['question'],
                        'sub_id' => $_POST['subject']
                    );
           $result = $this->ibps_admin->insert('questions',$data);
           if($result == true){
                $this->session->set_flashdata('message_name', 'Question Added Successfully');
                redirect(base_url().'admin/question');
           }else{
                $this->session->set_flashdata('message_name', 'Failed to add Question');
                redirect(base_url().'admin/question');
           }
    }
    public function dashboard(){
        $this->load->library('session');
        $this->load->model('ibps_admin');
        $data['data'] = $this->ibps_admin->select('subject');
        $this->load->view('admin/header');
        $this->load->view('admin/dashboard',$data);
    }

    public function update($id){
        $data = array(
            'sub_name' => $this->input->post('subject') );
            $this->load->model('ibps_admin');
            $this->ibps_admin->update_data('subject',$id,$data);
            redirect(base_url().'admin/dashboard');

    //print_r($_POST);
    }
    public function edit_sub($id){
        $this->load->model('ibps_admin');
        $result ['data']= $this->ibps_admin->get_by_id('subject',$id);
        $this->load->view('admin/header');
        $this->load->view('admin/edit_subject',$result);

    }
    public function delete_sub($id){
        $this->load->model('ibps_admin');
        $this->ibps_admin->delete('subject',$id);
        redirect(base_url().'admin/dashboard');
    }
    public function showquestion(){
        $this->load->model('ibps_admin');
        $data['data'] = $this->ibps_admin->select2();
        
        $this->load->view('admin/header');
        $this->load->view('admin/question_list',$data);
    }
    public function edit_questions($id){
        $this->load->model('ibps_admin');
        $questions= $this->ibps_admin->get_by_id('questions',$id);
        $subject = $this->ibps_admin->select('subject');
        $data=['questions'=>$questions,
                        'subject'=>$subject
                    ];
                   // print_r($data);exit;
        $this->load->view('admin/header');
        $this->load->view('admin/edit_question',$data);
    }
    
    public function updateQuestion($id){
        $data = array(
            'question' => $_POST['question'],
            'sub_id' => $_POST['subject']
             );
            $this->load->model('ibps_admin');
            $this->ibps_admin->update_data('questions',$id,$data);
            redirect(base_url().'admin/showquestion');
    }
    public function delete_questions($id){
        $this->load->model('ibps_admin');
        $this->ibps_admin->delete('questions',$id);
        redirect(base_url().'admin/showquestion');
    }
 
}